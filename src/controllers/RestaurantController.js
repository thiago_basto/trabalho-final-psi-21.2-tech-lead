const sequelize = require('../config/sequelize');
const { response } = require('express');
const Restaurant = require('../models/Restaurant');

const index = async(req,res) => {
    try {
        const restaurants = await Restaurant.findAll();
        return res.status(200).json({restaurants});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req, res) => {
    const {id} = req.params;
    try{
        const restaurant = await Restaurant.findByPk(id);
        return res.status(200).json({restaurant});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try {
          const restaurant = await Restaurant.create(req.body);
          return res.status(201).json({message: "Restaurante cadastrado com sucesso!", restaurant: restaurant});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Restaurant.update(req.body, {where: {id: id}});
        if(updated) {
            const restaurant = await Restaurant.findByPk(id);
            return res.status(200).send(restaurant);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Restaurante não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Restaurant.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Restaurante deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Restaurante não encontrado.");
    }
};

module.exports = {
    index,
    create,
    show,
    update,
    destroy,
};
