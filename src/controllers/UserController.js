const sequelize = require('../config/sequelize');
const { response } = require('express');
const User = require('../models/User');
const Restaurant = require('../models/Restaurant');
const { getFood } = require("./FoodController");
const Auth = require("../config/auth");

const index = async(req,res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try {
        const generateHash = Auth.generateHash(req.body.password);
        const salt = generateHash.salt;
        const hash = generateHash.hash;
        const food = await getFood();

        const newUserData = {
            email: req.body.email,
            salt: salt,
            hash: hash,
            name: req.body.name,
            phone_number: req.body.phone_number ? req.body.phone_number : null,
            address_code: req.body.address_code ? req.body.address_code : null,
            favorite_food: food ? food : null,
        }
        const user = await User.create(newUserData);

          return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuario deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuario não encontrado.");
    }
};

const userFavoriteFood = async(req,res) => {
    const {id} = req.params;
    try {
        const food = await getFood();
        const [updated] = await User.update({favorite_food: food}, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuario não encontrado.");
    }
};

const userChooseRestaurant = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const restaurant = await Restaurant.findByPk(req.body.RestaurantId);
        await user.setRestaurant(restaurant);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
};

const userRejectRestaurant = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        await user.setRestaurant(null);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    userFavoriteFood,
    userChooseRestaurant,
    userRejectRestaurant,
};
