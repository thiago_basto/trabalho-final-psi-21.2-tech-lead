const axios = require('axios');

const foodApi = axios.create({
    baseURL: "https://api.edamam.com",
});

exports.getFood = async () => {
    let response = await foodApi.get(`/api/recipes/v2`, {
        params: {
            type: "public",
            app_id: process.env.APP_ID,
            app_key: process.env.APP_KEY,
            q: "chicken",
            random: true,
        }
    });
    return response.data.hits[0].recipe.label;
}