const { Router } = require('express');
const UserController = require('../controllers/UserController');
const RestaurantController = require('../controllers/RestaurantController');
const AuthController = require('../controllers/AuthController');


const router = Router();

router.post('/login', AuthController.login);

router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users',UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);
router.put('/usersFavoriteFood/:id', UserController.userFavoriteFood);
router.put('/usersChooseRestaurant/:id', UserController.userChooseRestaurant);
router.delete('/usersRejectRestaurant/:id', UserController.userRejectRestaurant);

router.get('/restaurants',RestaurantController.index);
router.post('/restaurants',RestaurantController.create);
router.get('/restaurants/:id',RestaurantController.show);
router.put('/restaurants/:id',RestaurantController.update);
router.delete('/restaurants/:id', RestaurantController.destroy);

module.exports = router;

