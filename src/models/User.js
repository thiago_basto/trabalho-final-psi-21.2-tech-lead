const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull: false,
		unique: true
    },
    salt: {
        type: DataTypes.STRING
    },
    hash: {
        type: DataTypes.STRING
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    address_code: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
    phone_number: {
        type: DataTypes.STRING,
        allowNull: false
    },
    favorite_food: {
        type: DataTypes.STRING,
    },
});

User.associate = function(models) {
    User.belongsTo(models.Restaurant);
  }
  

module.exports = User;
