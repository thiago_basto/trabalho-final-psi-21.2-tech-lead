const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Restaurant = sequelize.define('Restaurant', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    street: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone_number: {
        type: DataTypes.STRING,
        allowNull: false
    },
});

Restaurant.associate = function(models) {
    Restaurant.hasMany(models.User);
}

module.exports = Restaurant;
