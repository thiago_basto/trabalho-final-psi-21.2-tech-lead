require('../../src/config/sequelize');
const request = require('supertest');
const app = require('../../src/app');
const User = require("../../src/models/User");
const faker = require('faker-br');
const Auth = require('../../src/config/auth');

describe("User CRUD", () => {  
    it("should create a new user when the the route '/users' is called with POST" , async () => {
        const user = {
            email: faker.internet.email(),
            name: faker.name.firstName(),
            address_code: faker.address.zipCode(),
            phone_number: faker.phone.phoneNumber(),
            password:'senha',
            favorite_food: 'feijão'
        };
        
            const response = await request(app).post('/users').send(user);
            expect(response.status).toBe(201);
            await User.destroy({where:{id:response.body.user.id}});
    });

    it("should update a user when the the route '/users/:id' is called with PUT" , async () => {
        const generateHash = Auth.generateHash('senha');
        const salt = generateHash.salt;
        const hash = generateHash.hash;
        const userData = {
            email: faker.internet.email(),
            hash: hash,
            salt: salt,
            name: faker.name.firstName(),
            address_code: faker.address.zipCode(),
            phone_number: faker.phone.phoneNumber(),
            password:'senha',
            favorite_food: 'feijão'
        };
        const user = await User.create(userData);
        const response = await request(app).put('/users/'+user.id).send({email:'email@teste.com'});
        expect(response.status).toBe(200);
        await User.destroy({where: {id: user.id}});
    });

    it("should list all users when the the route '/users' is called with GET" , async () => {
        const users = [];
        for(let i = 0; i<5;i++){
            const generateHash = Auth.generateHash('senha');
            const salt = generateHash.salt;
            const hash = generateHash.hash;
            const userData = {
                email: faker.internet.email(),
                hash: hash,
                salt: salt,
                name: faker.name.firstName(),
                address_code: faker.address.zipCode(),
                phone_number: faker.phone.phoneNumber(),
                password:'senha',
                favorite_food: 'feijão'
            };
            const user = await User.create(userData);
            users.push(user);
        }
        
        const response = await request(app).get('/users').send();
        users.forEach(async (user) => await User.destroy({where:{id:user.id}}));
        expect(response.status).toBe(200);
    });

    it("should show info about one user when the the route '/users/:id' is called with GET" , async () => {
        const generateHash = Auth.generateHash('senha');
        const salt = generateHash.salt;
        const hash = generateHash.hash;
        const userData = {
            email: faker.internet.email(),
            hash: hash,
            salt: salt,
            name: faker.name.firstName(),
            address_code: faker.address.zipCode(),
            phone_number: faker.phone.phoneNumber(),
            password:'senha',
            favorite_food: 'feijão'
        };
        const user = await User.create(userData);
        
        const response = await request(app).get('/users/'+user.id).send();

        expect(response.status).toBe(200);
        await User.destroy({where: {id: user.id}});
    });

    it("should delete the user when the the route '/users/:id' is called with DELETE" , async () => {
        const generateHash = Auth.generateHash('senha');
        const salt = generateHash.salt;
        const hash = generateHash.hash;
        const userData = {
            email: faker.internet.email(),
            hash: hash,
            salt: salt,
            name: faker.name.firstName(),
            address_code: faker.address.zipCode(),
            phone_number: faker.phone.phoneNumber(),
            password:'senha',
            favorite_food: 'feijão'
        };
        const user = await User.create(userData);

        const response = await request(app).delete('/users/'+user.id).send();
        expect(response.status).toBe(200);
    });
});
