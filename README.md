# Projeto Final Thiago Tech Lead | EJCM  

Esse é o repositório da minha aplicação para o PSI de Tech Lead na EJCM. A aplicação foi feita no NodeJs e possui um banco de dados em SQLite e com isso foi possível criar uma API RESTful que permiter relacionar usuários a restaurantes.

Além disso, um usuário consegue escolher uma comida favorita por meio da API de receitas Edaman, que usa autenticação por meio de Api Key no header.

**Status do Projeto** : Terminado.

![Badge](https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white) ![Badge](https://img.shields.io/badge/SQLite-07405E?style=for-the-badge&logo=sqlite&logoColor=white)

## Tabela de Conteúdo

1. [Tecnologias utilizadas](#tecnologias-utilizadas)
2. [Instalação](#instalação)
3. [Configuração](#configuração)
4. [Uso](#uso)
5. [Testes](#teste)
6. [Autores](#autores)

## Tecnologias utilizadas

- [NodeJS](https://nodejs.org/en//), *versão ^14.16.1*

## Instalação

- Clone o repositório;
- No Git Bash, vá para a pasta '**Trabalho Final**' e rode o seguinte comando no terminal:

``` bash
$ npm install
```

## Configuração

- copie o arquivo '.env.example' para o '.env' dando o seguinte comando:
``` bash
$ cp .env.example .env
```
- Rode o comando a seguir na pasta '**Trabalho Final**':

``` bash
$ npm run migrate
```

## Uso

Para interagir com a aplicação será preciso servir o Back-End do projeto.

- Rode o seguinte comando para servir o backend:

``` bash
$ npm run dev
```

## Testes

Os testes da aplicação são feitos pelo Jest, sua configuração é feita no arquivo '**jest.config.js**' e os testes estão localizados no arquivo '**\__tests__/unit/user.unit.test.js**'.

No comando para rodar os testes foi adicionado duas configurações. A primeira foi o --forceExit, porque o teste rodava infinitamente, e a outra foi o --testTimeout=10000, por causa do teste de criar um usuário que utiliza uma API externa e estava fazendo a requisição demorar mais e causar erro de timeout.

Para rodar o teste basta usar o comando abaixo no terminal, isso porquê no arquivo '**package.json**' foi feito um script que roda esse comando ao invés de **"jest --forceExit --testTimeout=10000"**
``` bash
$ npm run test
```

## Autores


> Período: Desenvolvimento

* Thiago Basto - Tech Lead

## Última atualização: 06/08/2021
